# QACubeInternship



### Instructions
Technologies used:<br />
  - Backend: Java + Spring Boot<br />
  - Frontend: Angular<br />
  - RDBMS: MySQL<br />

Installation requirements:<br />
  - NodeJS<br />
  - MySQL<br />
  - Angular<br />

Project requirements<br />
  - Clone project from https://gitlab.com/maja18/qacubeinternship<br />


Database: MySQL
  - Password: root, username: root

Backend:
  - Import project in Intellij
  - Run project
  - Backend is on port 8080

Frontend:
  - In node js cmd position to frontend folder (example: cd Desktop/qacubeinternship/frontend)
  - Then run:
```sh
                                $ npm install
                                $ ng serve
- Frontend will be on port `4200`


import { Deserializable } from "./deserializable.models";

export class User implements Deserializable{
    id: number;
    email: string;
    password: string;
    name: string;

    constructor(){
        this.id = 0;
        this.email = '';
        this.password = '';
        this.name = ''
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}


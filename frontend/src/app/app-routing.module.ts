import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddApplicationComponent } from './components/add-application/add-application.component';
import { AddConnectionComponent } from './components/add-connection/add-connection.component';
import { ApplicationComponent } from './components/application/application.component';
import { CommitComponent } from './components/commit/commit.component';
import { ConectionComponent } from './components/conection/conection.component';
import { EditApplicationComponent } from './components/edit-application/edit-application.component';
import { EditConnectionComponent } from './components/edit-connection/edit-connection.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MergeRequestComponent } from './components/merge-request/merge-request.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'application', component: ApplicationComponent },
  { path: 'connection', component: ConectionComponent },
  { path: 'application/edit/:id', component: EditApplicationComponent},
  { path: 'connection/new', component: AddConnectionComponent},
  { path: 'application/new', component: AddApplicationComponent},
  { path: 'connection/edit/:id', component: EditConnectionComponent},
  { path: 'mergeRequests', component:MergeRequestComponent},
  { path: 'commits', component:CommitComponent},
  { path: 'home', component:HomeComponent},
  { path: 'login', component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

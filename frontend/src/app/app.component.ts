import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend';
  isLogged !: boolean

  gapi: any;//this line should be at the very top of your TS file
  auth2: any;

  constructor(private loginService: LoginService) { }

  ngOnInit(){
    this.isUserLogged()
  }

  logout() {
    this.loginService.signOut();
    window.location.reload();
  }

  isUserLogged(){
    this.isLogged = this.loginService.isUserLogged();
  }
  
}

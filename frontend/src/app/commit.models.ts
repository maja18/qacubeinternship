import { Deserializable } from "./deserializable.models";

export class Commit implements Deserializable {
    author_name: string;
    short_id: string;
    message : string;
    created_at : string;
    created : Date;
    
    constructor(){
       this.author_name = ""
       this.short_id = ""
       this.message = ""
       this.created_at = ""
       this.created = new Date
    }

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
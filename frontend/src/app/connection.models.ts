import { Deserializable } from "./deserializable.models";

export class Connection implements Deserializable {
    id: number;
    name: string;
    url: string;
    token: string;
    branchName: string;
    project: string;
    checked?: boolean;

    constructor(){
        this.id = 0;
        this.name = "";
        this.url = "";
        this.token = "";
        this.branchName = "";
        this.project = "";
        this.checked = false;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
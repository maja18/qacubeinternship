import { Connection } from './connection.models';
import { Deserializable } from './deserializable.models';

export class Application implements Deserializable{
    id: number;
    name: string;
    connections: Connection[]

    constructor(){
      this.id = 0;
      this.name = "";
      this.connections = []
    }

    deserialize(input: any) {
      Object.assign(this, input);
      return this;
    }
  }
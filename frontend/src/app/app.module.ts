import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApplicationComponent } from './components/application/application.component';
import { ConectionComponent } from './components/conection/conection.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditApplicationComponent } from './components/edit-application/edit-application.component';
import { AddApplicationComponent } from './components/add-application/add-application.component';
import { AddConnectionComponent } from './components/add-connection/add-connection.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditConnectionComponent } from './components/edit-connection/edit-connection.component';
import { MergeRequestComponent } from './components/merge-request/merge-request.component';
import { CommitComponent } from './components/commit/commit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HomeComponent } from './components/home/home.component';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { LoginComponent } from './components/login/login.component';
import { WebSocketService } from './services/web-socket.service';

@NgModule({
  declarations: [
    AppComponent,
    ApplicationComponent,
    ConectionComponent,
    EditApplicationComponent,
    AddApplicationComponent,
    AddConnectionComponent,
    EditConnectionComponent,
    MergeRequestComponent,
    CommitComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    BrowserAnimationsModule,
	  ToastrModule.forRoot(),
    SocialLoginModule,
    ReactiveFormsModule,
  ],
  providers: [WebSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }

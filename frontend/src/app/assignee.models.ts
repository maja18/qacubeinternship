import { Deserializable } from "./deserializable.models";

export class Assignee implements Deserializable {
    name: String;
    
    constructor(){
       this.name = ""
    }

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
import { Component, OnInit } from '@angular/core';
import { Connection } from '../../connection.models';
import { ConnectionService } from '../../services/connection.service';
import { Router } from '@angular/router';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-conection',
  templateUrl: './conection.component.html',
  styleUrls: ['./conection.component.css']
})
export class ConectionComponent implements OnInit {
  public hideContent:boolean[] = [];
  public buttonName:any = 'Expand';
  connections: Connection[] = []
  closeModal!: string;
  query = '';

  constructor( private connectionService: ConnectionService, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.query = '{ allConnections {id name url project branchName} }';
    this.connectionService.getConnectionsGraphQL(this.query).subscribe((response: any) => {
      this.connections = response.data.allConnections;
    }, (err: any) => {
      console.log(err.error.message);
    });
  }

  toggle(index: any) {
    // toggle based on index
    this.hideContent[index] = !this.hideContent[index];
  }

  goToNewConnection($myParam: string = ''): void {
    const navigationDetails: string[] = ['/connection/new'];
    if($myParam.length) {
      navigationDetails.push($myParam);
    }
    this.router.navigate(navigationDetails);
  }

  deleteConnection(connection: Connection){
    this.connections = this.connections.filter(c => c !== connection);
    this.connectionService.deleteConnection(connection.id)
    .subscribe()
  }

  triggerModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}

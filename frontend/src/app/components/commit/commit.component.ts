import { Component, OnInit } from '@angular/core';
import { Application } from 'src/app/application.models';
import { Commit } from 'src/app/commit.models';
import { Connection } from 'src/app/connection.models';
import { ApplicationService } from 'src/app/services/application.service';
import { CommitService } from 'src/app/services/commit.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-commit',
  templateUrl: './commit.component.html',
  styleUrls: ['./commit.component.css']
})
export class CommitComponent implements OnInit {
  public hideContent:boolean[] = [];
  applications: Application[] = []
  appConnections : Connection[] = []
  choosenConnection !: Connection
  commits : Commit[] = []
  choosenApplication: any;
  isApplicationEmpty: boolean = false;
  choosenConnectionDropdown: any;
  isConnectionEmpty: boolean = false;

  constructor(private applicationService: ApplicationService, private commitService : CommitService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.applicationService.getApplications()
      .subscribe(applications => this.applications = applications);
  }

  findAppConnections(application: Application){
    this.appConnections = application.connections
  }

  rememberConnection(connection: Connection){
    this.choosenConnection = connection
  }

  toggle(index: any) {
    // toggle based on index
    this.hideContent[index] = !this.hideContent[index];
  }

  getCommits(){
    this.validateApplicationDropdown()
    this.validateConnectionDropdown()
    if (!this.isApplicationEmpty || !this.isConnectionEmpty){
      this.commitService.getCommits(this.choosenConnection.id)
      .subscribe(commits => {    
        this.commits = commits
        this.commits.forEach(c => {
        let date:Date = new Date(c.created_at.toString())
        c.created = new Date()
        let number = date.getDate();
        c.created.setDate(number)
        c.created.setHours(date.getHours())
        c.created.setMinutes(date.getMinutes())
      })
    },
    error => {
      this.showErrorToaster(error.error.message);
    })
    }
  }

  validateApplicationDropdown() {
    if (this.choosenApplication === undefined) {
      this.isApplicationEmpty = true;
    } else {
      this.isApplicationEmpty = false;
    }
  }

  validateConnectionDropdown() {
    if (this.choosenConnectionDropdown === undefined) {
      this.isConnectionEmpty = true;
    } else {
      this.isConnectionEmpty = false;
    }
  }

  showErrorToaster(message:any){
    this.toastr.error(message)
  }

  changeConnection(connection: Connection){
    this.choosenConnectionDropdown = connection.name
  }

  changeApplication(application: Application){
    this.choosenApplication = application.name
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Application } from 'src/app/application.models';
import { ApplicationService } from 'src/app/services/application.service';
import { Connection } from '../../connection.models';
import { ConnectionService } from '../../services/connection.service';

@Component({
  selector: 'app-add-application',
  templateUrl: './add-application.component.html',
  styleUrls: ['./add-application.component.css']
})
export class AddApplicationComponent implements OnInit {
  application = new Application
  connections: Connection[] = []

  @ViewChild('myForm', { static: false })
  myForm!: NgForm;
  choosenConnection: any;
  isOperatorEmpty: boolean = false;

  constructor( private connectionService : ConnectionService, private applicationService: ApplicationService,
    private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.connectionService.getAvailableConnections()
      .subscribe(connections => this.connections = connections)
  }

  addApplication(): void {
    this.validateConnectionDropdown()
    if (!this.isOperatorEmpty){
    this.applicationService.addApplication(this.application)
      .subscribe(application => {
        this.showSuccessToaster("Successfuly added application");
        this.myForm.resetForm();
        this.application.connections = []
        this.router.navigate(['/application'])
      });
    }
  } 
  
  addConnectionToApplication(connection: Connection) {
    this.application.connections.push(connection);
  }

  validateConnectionDropdown() {
    if (this.choosenConnection === undefined) {
      this.isOperatorEmpty = true;
    } else {
      this.isOperatorEmpty = false;
    }
  }

  showSuccessToaster(message:any){
    this.toastr.success(message)
  }

  cancel(){
    this.router.navigate(['/application'])
  }

  changeConnection(connection: Connection){
    this.choosenConnection = connection.name
  }

}

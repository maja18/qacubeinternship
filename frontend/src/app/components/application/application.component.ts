import { Component, OnInit } from '@angular/core';
import { Application } from '../../application.models';
import { ApplicationService } from '../../services/application.service';
import { Router } from '@angular/router';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  public hideContent:boolean[] = [];
  public buttonName:any = 'Expand';
  applications: Application[] = []
  displayStyle = "none";
  closeModal!: string;
  query = '';

  constructor( private applicationService: ApplicationService, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.query = '{ allApplications {id name connections {id name}} }';
    this.applicationService.getApplicationsGraphQL(this.query).subscribe((response: any) => {
      this.applications = response.data.allApplications;
    }, (err: any) => {
      console.log(err.error.message);
    });
  }

  toggle(index: any) {
    // toggle based on index
    this.hideContent[index] = !this.hideContent[index];
  }
  
  openPopup() {
    this.displayStyle = "block";
  }

  closePopup() {
    this.displayStyle = "none";
  }

  goToNewApplication($myParam: string = ''): void {
    const navigationDetails: string[] = ['/application/new'];
    if($myParam.length) {
      navigationDetails.push($myParam);
    }
    this.router.navigate(navigationDetails);
  }

  deleteApplication(application: Application){
    this.applications = this.applications.filter(a => a !== application);
    this.applicationService.deleteApplication(application.id)
    .subscribe()
  }

  triggerModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}

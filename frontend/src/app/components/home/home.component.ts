import { Component, OnInit } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { WebSocketService } from 'src/app/services/web-socket.service';
import {RxStomp} from "@stomp/rx-stomp";
import { map } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public notificationsNumber = 0;
  message!: string
  private client!: RxStomp;
  public notifications: string[] = [];
  showNotifications: boolean = false
  hideContent: boolean = true
  

  constructor(private webSocketService: WebSocketService) { 
    if (!this.client || this.client.connected()) {
      this.client = new RxStomp();
      this.client.configure({
        webSocketFactory: () => new SockJS('http://localhost:8080/api/notifications'),
        debug: (msg: string) => console.log(msg)
      });
      this.client.activate();

      this.watchForNotifications();

      console.info('connected!');
    }
  }

  ngOnInit(): void {
  }

  private watchForNotifications() {
    this.client.watch('/user/notification/item')
      .pipe(
        map((response) => {
          const message: string = JSON.parse(response.body).message;
          console.log('Got ' + message);
          return message;
        }))
      .subscribe((notification: string) => this.notifications.push(notification));
  }

  disconnectClicked() {
    if (this.client && this.client.connected()) {
      this.client.deactivate();
      //this.client = null;
      console.info("disconnected :-/");
    }
  }

  startClicked() {
    if (this.client && this.client.connected()) {
      this.client.publish({destination: '/swns/start'});
    }
  }

  stopClicked() {
    if (this.client && this.client.connected()) {
      this.client.publish({destination: '/swns/stop'});
    }
  }

  seeNotifications(){
    this.showNotifications = true;
  }

  toggle() {
    // toggle based on index
    this.hideContent = !this.hideContent;
  }

}

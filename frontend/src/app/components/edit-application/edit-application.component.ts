import { Component, OnInit, ViewChild } from '@angular/core';
import { Application } from '../../application.models';
import { ApplicationService } from '../../services/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectionService } from '../../services/connection.service';
import { Connection } from '../../connection.models';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-application',
  templateUrl: './edit-application.component.html',
  styleUrls: ['./edit-application.component.css']
})
export class EditApplicationComponent implements OnInit {

  application !: Application;
  connections: Connection[] = []
  public hideContent:boolean = false;
  connection !: Connection
  conn !:Connection
  c !: Connection

  constructor(private applicationService: ApplicationService,private route: ActivatedRoute, private connectionService: ConnectionService,
    private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.applicationService.getApplication(id)
      .subscribe(application => this.application = application);

      this.connectionService.getConnections()
      .subscribe(connections =>
        {
          this.connections = connections.filter(c => !this.application.connections.find(conn => conn.id === c.id))
        })

  }

  toggle() {
    // toggle based on index
    this.hideContent = !this.hideContent;
  }

  editApplication(): void {
    this.selectedCheckboxList.forEach(c => {
      this.application.connections.push(c)
    })
    this.applicationService.editApplication(this.application)
      .subscribe(application => {
        this.showSuccessToaster("Successfuly edited application");
      });
      this.connections = this.connections.filter(c => !this.application.connections.find(conn => conn.id === c.id))
  }  

  get selectedCheckboxList() {
    return this.connections.filter(item => item.checked);
  }

  removeConnection(connection: Connection){
    this.application.connections.forEach((c, index) =>{
      if (c === connection){
        this.application.connections.splice(index, 1)
        this.applicationService.removeConnectionFromApplication(c)
        .subscribe()
        this.connections.push(c)
      }
    })
  }

  showSuccessToaster(message:any){
    this.toastr.success(message)
  }

  cancel(){
    this.router.navigate(['/application'])
  }
}

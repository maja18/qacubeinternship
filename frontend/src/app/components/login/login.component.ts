import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Obj } from '@popperjs/core';
import { GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from 'src/app/services/login.service';
import { User } from 'src/app/user.models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  auth2: any;
  registerForm:any = FormGroup;
  submitted = false;
  user = new User

  
  @ViewChild('loginRef', { static: true }) loginElement!: ElementRef;
  @ViewChild('myForm', { static: false })
  myForm!: NgForm;

  constructor(private router: Router, private toastr: ToastrService, private formBuilder: FormBuilder, private loginService: LoginService) { }
  get f() { return this.registerForm.controls; }

  onSubmit() {
  
    this.submitted = true;
    // stop here if form is invalid
    if (this.myForm.invalid) {
        return;
    }
    //True if all the fields are filled
    if(this.submitted)
    {
      this.loginService.login(this.user)
      .subscribe(application => {
        this.showSuccessToaster("Successfuly logged");
        sessionStorage.setItem("user", this.user.email)
        this.router.navigate(['/home']).then(() => {
          window.location.reload();
        });
        this.showSuccessToaster(this.user.name + ", welcome back");
      },
      error => {
        this.showErrorToaster(error.error.message);
      })
    }
   
  }
  
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
      });

    this.googleAuthSDK();
  }

  callLogin() {

    this.auth2.attachClickHandler(this.loginElement.nativeElement, {},
      (googleAuthUser: any) => {
        let profile = googleAuthUser.getBasicProfile();
        console.log('Token || ' + googleAuthUser.getAuthResponse().id_token);
        console.log('ID: ' + profile.getId());
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail());
        alert(profile.getName() + ", welcome back");
        this.showSuccessToaster(profile.getName() + ", welcome back");
        //ovde treba token da posaljes na backend
        this.loginService.loginGoogle(googleAuthUser.getAuthResponse().id_token)
         .subscribe((response: any) => {
          this.user = new User().deserialize(response);
          sessionStorage.setItem('user', this.user.email);
          this.router.navigate(['/home']).then(() => {
            window.location.reload();
          });
        });
  
      }, (error: any) => {
        this.showErrorToaster(JSON.stringify(error, undefined, 2));
      });

  }

  googleAuthSDK() {

    (<any>window)['googleSDKLoaded'] = () => {
      (<any>window)['gapi'].load('auth2', () => {
        this.auth2 = (<any>window)['gapi'].auth2.init({
          client_id: '600786407464-llspkfklmqtirmmk3vuci8nanqlo1vd1.apps.googleusercontent.com',
          plugin_name:'login',
          cookiepolicy: 'single_host_origin',
          scope: 'profile email'
        });
        this.callLogin();
      });
    }

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement('script');
      js.id = id;
      js.src = "https://apis.google.com/js/platform.js?onload=googleSDKLoaded";
      fjs?.parentNode?.insertBefore(js, fjs);
    }(document, 'script', 'google-jssdk'));
  }  

  showSuccessToaster(message:any){
    this.toastr.success(message)
  }

  showErrorToaster(message:any){
    this.toastr.error(message)
  }
}

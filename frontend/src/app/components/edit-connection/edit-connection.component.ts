import { Component, OnInit, ViewChild } from '@angular/core';
import { Connection } from '../../connection.models';
import { ConnectionService } from '../../services/connection.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-connection',
  templateUrl: './edit-connection.component.html',
  styleUrls: ['./edit-connection.component.css']
})
export class EditConnectionComponent implements OnInit {

  @ViewChild('myForm', { static: false })
  myForm!: NgForm;
  connection!: Connection
  connections: Connection[] = []

  constructor(private connectionService: ConnectionService, private route: ActivatedRoute, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.connectionService.getConnection(id)
      .subscribe(connection => this.connection = connection);
  }

  editConnection(): void {
    this.connectionService.editConnection(this.connection)
      .subscribe(connection => {
        this.connections.push(connection);
        this.showSuccessToaster("Successfuly edited connection");
        this.router.navigate(['/connection'])
      });
  }  

  showSuccessToaster(message:any){
    this.toastr.success(message)
  }

  cancel(){
    this.router.navigate(['/connection'])
  }
}

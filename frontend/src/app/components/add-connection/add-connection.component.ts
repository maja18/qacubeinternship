import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Connection } from '../../connection.models';
import { ConnectionService } from '../../services/connection.service';

@Component({
  selector: 'app-add-connection',
  templateUrl: './add-connection.component.html',
  styleUrls: ['./add-connection.component.css']
})
export class AddConnectionComponent implements OnInit {
  connections: Connection[] = []
  connection = new Connection 
  
  @ViewChild('myForm', { static: false })
  myForm!: NgForm;

  constructor( private connectionService: ConnectionService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  addConnection(): void {
    this.connectionService.addConnection(this.connection)
      .subscribe(connection => {
        this.connections.push(connection);
        this.showSuccessToaster("Successfuly added connection");
        this.myForm.resetForm();
        this.router.navigate(['/connection'])
      });
  } 
  
  showSuccessToaster(message:any){
    this.toastr.success(message)
  }

  cancel(){
    this.router.navigate(['/connection'])
  }
}

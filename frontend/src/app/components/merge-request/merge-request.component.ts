import { Component, OnInit } from '@angular/core';
import { Connection } from 'src/app/connection.models';
import { MergeRequest } from 'src/app/mergeRequest.models';
import { MergeRequestService } from 'src/app/services/merge-request.service';
import { Application } from '../../application.models';
import { ApplicationService } from '../../services/application.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-merge-request',
  templateUrl: './merge-request.component.html',
  styleUrls: ['./merge-request.component.css']
})
export class MergeRequestComponent implements OnInit {

  applications: Application[] = []
  appConnections : Connection[] = []
  choosenConnection !: Connection
  mergeRequests : MergeRequest[] = []
  public hideContent:boolean[] = [];
  date!: Date;
  choosenApplication: any;
  isApplicationEmpty: boolean = false;
  choosenConnectionDropdown: any;
  isConnectionEmpty: boolean = false;

  constructor(private applicationService: ApplicationService, private mergeRequestService: MergeRequestService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.applicationService.getApplications()
      .subscribe(applications => this.applications = applications);
  }

  findAppConnections(application: Application){
    this.appConnections = application.connections
  }

  rememberConnection(connection: Connection){
    this.choosenConnection = connection
  }

  getMergeRequests(){
    this.validateApplicationDropdown()
    this.validateConnectionDropdown()
    if (!this.isApplicationEmpty || !this.isConnectionEmpty){
      this.mergeRequestService.getMergeRequests(this.choosenConnection.id)
      .subscribe(mergeRequests => 
        {
        this.mergeRequests = mergeRequests
        this.mergeRequests.forEach(mr => {
        let date:Date = new Date(mr.created_at.toString())
        mr.created = new Date()
        let number = date.getDate();
        mr.created.setDate(number)
        mr.created.setHours(date.getHours())
        mr.created.setMinutes(date.getMinutes())
        })
      },
      error => {
        this.showErrorToaster(error.error.message);
      })
    }
  }

  toggle(index: any) {
    // toggle based on index
    this.hideContent[index] = !this.hideContent[index];
  }

  validateApplicationDropdown() {
    if (this.choosenApplication === undefined) {
      this.isApplicationEmpty = true;
    } else {
      this.isApplicationEmpty = false;
    }
  }

  validateConnectionDropdown() {
    if (this.choosenConnectionDropdown === undefined) {
      this.isConnectionEmpty = true;
    } else {
      this.isConnectionEmpty = false;
    }
  }

  showErrorToaster(message:any){
    this.toastr.error(message)
  }

  changeConnection(connection: Connection){
    this.choosenConnectionDropdown = connection.name
  }

  changeApplication(application: Application){
    this.choosenApplication = application.name
  }
}

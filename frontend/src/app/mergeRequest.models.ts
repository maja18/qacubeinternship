import { Assignee } from "./assignee.models";
import { Author } from "./author.models";
import { Deserializable } from "./deserializable.models";

export class MergeRequest implements Deserializable {
    title: String;
    author: Author;
    assignee: Assignee;
    created_at: String
    created: Date
    
    constructor(){
       this.title = ""
       this.author = new Author
       this.assignee = new Assignee
       this.created_at = ""
       this.created = new Date
    }

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
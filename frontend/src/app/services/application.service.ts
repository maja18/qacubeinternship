import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Application } from '../application.models';
import { Connection } from '../connection.models';


@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  private appsUrl = 'http://localhost:8080/api/application/applications';
  private url = 'http://localhost:8080/api/application';
  private url2 = 'http://localhost:8080/api/application/removeConnection';
  private url3 = 'http://localhost:8080/api/application/graphql';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getApplications(): Observable<Application[]> {
    return this.http.get<Application[]>(this.appsUrl)
  }

  getApplication<Data>(id: number): Observable<Application> {
    const url = `${this.url}/${id}`;
    return this.http.get<Application>(url)
  }

  addApplication(application: Application): Observable<Application> {
    return this.http.post<Application>(this.url, application, this.httpOptions);
  }

  deleteApplication(id: Number): Observable<Application>{
    const url = `${this.url}/${id}`;
    return this.http.delete<Application>(url, this.httpOptions);
  }

  editApplication(application: Application): Observable<Application> {
    return this.http.put<Application>(this.url, application, this.httpOptions);
  }

  removeConnectionFromApplication(connection: Connection): Observable<void>{
    return this.http.put<void>(this.url2, connection, this.httpOptions);
  }

  getApplicationsGraphQL(query: string): any {
    return this.http.post(this.url3, query);
  }

}

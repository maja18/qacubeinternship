import { Injectable } from '@angular/core';
import * as Rj from 'rxjs';

var SockJs = require("sockjs-client");
var Stomp = require("stompjs");

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  constructor() { }

 // Open connection with the back-end socket
 public connect() {
  let socket = new SockJs(`http://localhost:8080/api/socket`);

  let stompClient = Stomp.over(socket);

  return stompClient;
}
}

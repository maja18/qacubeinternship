import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Connection } from '../connection.models';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  private connectionUrl = 'http://localhost:8080/api/connection/connections';
  private url = 'http://localhost:8080/api/connection';
  private availableConnectionUrl = 'http://localhost:8080/api/connection/availableConnections';
  private url3 = 'http://localhost:8080/api/connection/graphql';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getConnections(): Observable<Connection[]> {
    return this.http.get<Connection[]>(this.connectionUrl)
  }

  addConnection(connection: Connection): Observable<Connection> {
    return this.http.post<Connection>(this.url, connection, this.httpOptions);
  }

  deleteConnection(id: Number): Observable<Connection>{
    const url = `${this.url}/${id}`;
    return this.http.delete<Connection>(url, this.httpOptions);
  }

  getConnection<Data>(id: number): Observable<Connection> {
    const url = `${this.url}/${id}`;
    return this.http.get<Connection>(url)
  }

  editConnection(connection: Connection): Observable<Connection> {
    return this.http.put<Connection>(this.url, connection, this.httpOptions);
  }

  getAvailableConnections(): Observable<Connection[]> {
    return this.http.get<Connection[]>(this.availableConnectionUrl)
  }

  getConnectionsGraphQL(query: string): any {
    return this.http.post(this.url3, query);
  }
}

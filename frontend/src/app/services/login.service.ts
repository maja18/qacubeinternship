import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../user.models';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url = 'http://localhost:8080/api/auth/login';
  private url2 = 'http://localhost:8080/api/auth/loginGoogle';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  login(user: User): Observable<User> {
    return this.http.post<User>(this.url, user, this.httpOptions);
  }

  isUserLogged(): boolean{
    const user = sessionStorage.getItem("user");
    return !(user == null)
  }

  loginGoogle(token: string) : Observable<Object> {
    return this.http.post<Object>(this.url2, token)
  }

  signOut(): void {
    window.sessionStorage.clear();
  }
  
}

import { TestBed } from '@angular/core/testing';

import { MergeRequestService } from './merge-request.service';

describe('MergeRequestService', () => {
  let service: MergeRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MergeRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

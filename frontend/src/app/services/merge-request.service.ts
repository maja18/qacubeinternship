import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MergeRequest } from '../mergeRequest.models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MergeRequestService {
  private url = 'http://localhost:8080/api/gitlab/merge-requests';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getMergeRequests(id:number): Observable<MergeRequest[]> {
    const url = `${this.url}/${id}`;
    return this.http.get<MergeRequest[]>(url)
  }
}

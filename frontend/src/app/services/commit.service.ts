import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Commit } from '../commit.models';

@Injectable({
  providedIn: 'root'
})
export class CommitService {

  private url = 'http://localhost:8080/api/gitlab/commits';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getCommits(id:number): Observable<Commit[]> {
    const url = `${this.url}/${id}`;
    return this.http.get<Commit[]>(url)
  }
}

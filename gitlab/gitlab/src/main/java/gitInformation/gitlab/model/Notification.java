package gitInformation.gitlab.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class Notification {
    private Long count;
    private String message;

    public Notification(Long count) {
        this.count = count;
    }
    public Notification(String message) {
        this.message = message;
    }

    public void increment() {
        this.count++;
    }
}

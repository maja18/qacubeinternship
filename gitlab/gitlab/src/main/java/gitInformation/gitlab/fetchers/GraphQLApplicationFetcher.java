package gitInformation.gitlab.fetchers;

import gitInformation.gitlab.dto.ApplicationDTO;
import gitInformation.gitlab.service.ApplicationServiceImpl;
import graphql.schema.DataFetcher;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class GraphQLApplicationFetcher {
    @Autowired
    private ApplicationServiceImpl applicationService;

    public DataFetcher<List<ApplicationDTO>> allApplications() {
        return dataFetchingEnvironment -> {
            List<ApplicationDTO> applications = applicationService.getAllApplications();
            return applications;
        };
    }
}

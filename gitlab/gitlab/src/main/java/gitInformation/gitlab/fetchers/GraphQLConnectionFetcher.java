package gitInformation.gitlab.fetchers;

import gitInformation.gitlab.dto.ApplicationDTO;
import gitInformation.gitlab.dto.ConnectionDTO;
import gitInformation.gitlab.service.ApplicationServiceImpl;
import gitInformation.gitlab.service.ConnectionServiceImpl;
import graphql.schema.DataFetcher;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class GraphQLConnectionFetcher {
    @Autowired
    private ConnectionServiceImpl connectionService;

    public DataFetcher<List<ConnectionDTO>> allConnections() {
        return dataFetchingEnvironment -> {
            List<ConnectionDTO> connections = connectionService.getAllConnections();
            return connections;
        };
    }
}

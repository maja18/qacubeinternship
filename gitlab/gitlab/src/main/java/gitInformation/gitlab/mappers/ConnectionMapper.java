package gitInformation.gitlab.mappers;

import gitInformation.gitlab.dto.ConnectionDTO;
import gitInformation.gitlab.model.Connection;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.FIELD
)
public interface ConnectionMapper {
    ConnectionDTO connectionToConnectionDTO(Connection connection);
    List<Connection> connectionDTOsToConnections(List<ConnectionDTO> connectionDTOs);
    Connection connectionDTOToConnection(ConnectionDTO connectionDTO);
    List<ConnectionDTO> connectionsToConnectionDTOs(List<Connection> connections);
}

package gitInformation.gitlab.mappers;

import gitInformation.gitlab.dto.PersonDTO;
import gitInformation.gitlab.model.Person;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.FIELD
)
public interface PersonMapper {
    PersonDTO personToPersonDTO(Person person);
}

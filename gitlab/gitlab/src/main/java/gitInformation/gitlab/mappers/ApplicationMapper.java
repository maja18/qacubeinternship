package gitInformation.gitlab.mappers;
import gitInformation.gitlab.dto.ApplicationDTO;
import gitInformation.gitlab.model.Application;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.FIELD
)
public interface ApplicationMapper {
    List<ApplicationDTO> applicationsToApplicationDTOs(List<Application> application);
    ApplicationDTO applicationToApplicationDTO(Application application);
}

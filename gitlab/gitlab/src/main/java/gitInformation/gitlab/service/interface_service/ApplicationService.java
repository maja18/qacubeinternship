package gitInformation.gitlab.service.interface_service;
import gitInformation.gitlab.dto.ApplicationDTO;
import gitInformation.gitlab.dto.ConnectionDTO;

import java.util.List;
public interface ApplicationService {
    List<ApplicationDTO> getAllApplications();
    ApplicationDTO addNewApplication(ApplicationDTO applicationDTO);
    ApplicationDTO deleteApplication(Long applicationId);
    ApplicationDTO editApplication(ApplicationDTO applicationDTO);
    ApplicationDTO getApplicationById(Long id);
    ConnectionDTO removeConnectionFromApplication(ConnectionDTO connectionDTO);
}

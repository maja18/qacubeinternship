package gitInformation.gitlab.service.interface_service;

import gitInformation.gitlab.dto.PersonDTO;

public interface PersonService {
    PersonDTO login(PersonDTO personDTO);
    PersonDTO processOAuthPostLogin(String email);
}

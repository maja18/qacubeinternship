package gitInformation.gitlab.service.interface_service;

import gitInformation.gitlab.dto.ConnectionDTO;

import java.util.List;

public interface ConnectionService {
    ConnectionDTO addNewConnection(ConnectionDTO connectionDTO);
    List<ConnectionDTO> getAllConnections();
    ConnectionDTO deleteConnection(Long id);
    ConnectionDTO editConnection(ConnectionDTO connectionDTO);
    ConnectionDTO getConnectionById(Long id);
    List<ConnectionDTO> getAllAvailableConnections();
}

package gitInformation.gitlab.service;

import gitInformation.gitlab.dto.ConnectionDTO;
import gitInformation.gitlab.exceptions.BadRequestException;
import gitInformation.gitlab.exceptions.ConnectionException;
import gitInformation.gitlab.mappers.ConnectionMapper;
import gitInformation.gitlab.model.Connection;
import gitInformation.gitlab.repository.ConnectionRepository;
import gitInformation.gitlab.service.interface_service.ConnectionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConnectionServiceImpl implements ConnectionService {
    private final ConnectionRepository connectionRepository;
    private final ConnectionMapper connectionMapper;

    @Override
    public ConnectionDTO addNewConnection(ConnectionDTO connectionDTO) {
        Connection connection = connectionMapper.connectionDTOToConnection(connectionDTO);
        connectionRepository.save(connection);
        return  connectionMapper.connectionToConnectionDTO(connection);
    }

    @Override
    public List<ConnectionDTO> getAllConnections() {
        List<Connection> connections = connectionRepository.findAll();
        return connectionMapper.connectionsToConnectionDTOs(connections);
    }

    @Override
    public ConnectionDTO deleteConnection(Long id) {
        Connection connection;
        try {
            connection = connectionRepository.findById(id).get();
        }catch (Exception e){
            throw new BadRequestException("Connection with given id does not exist!");
        }
        connectionRepository.delete(connection);

        return connectionMapper.connectionToConnectionDTO(connection);
    }

    @Override
    public ConnectionDTO editConnection(ConnectionDTO connectionDTO) {
        Connection connection = connectionRepository.findById(connectionDTO.getId()).get();
        if(connection != null){
            connection.setName(connectionDTO.getName());
            connection.setToken(connectionDTO.getToken());
            connection.setUrl(connectionDTO.getUrl());
            connection.setProject(connectionDTO.getProject());
            connection.setBranchName(connectionDTO.getBranchName());
            connectionRepository.save(connection);
        }else{
            throw new ConnectionException("Connection does not exist!");
        }

        return connectionMapper.connectionToConnectionDTO(connection);
    }

    @Override
    public ConnectionDTO getConnectionById(Long id) {
        Connection connection = connectionRepository.findById(id).get();

        return connectionMapper.connectionToConnectionDTO(connection);
    }

    @Override
    public List<ConnectionDTO> getAllAvailableConnections() {
        List<Connection> connections = connectionRepository.findAll();
        List<Connection> availableConnections = new ArrayList<>();
        connections.stream().forEach(c -> {
            if (c.getApplication() == null){
                availableConnections.add(c);
            }
        });

        return connectionMapper.connectionsToConnectionDTOs(availableConnections);
    }
}

package gitInformation.gitlab.service;
import gitInformation.gitlab.dto.ApplicationDTO;
import gitInformation.gitlab.dto.ConnectionDTO;
import gitInformation.gitlab.exceptions.BadRequestException;
import gitInformation.gitlab.exceptions.ConnectionException;
import gitInformation.gitlab.mappers.ApplicationMapper;
import gitInformation.gitlab.mappers.ConnectionMapper;
import gitInformation.gitlab.model.Application;
import gitInformation.gitlab.model.Connection;
import gitInformation.gitlab.repository.ApplicationRepository;
import gitInformation.gitlab.repository.ConnectionRepository;
import gitInformation.gitlab.service.interface_service.ApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {
    private final ApplicationRepository applicationRepository;
    private final ApplicationMapper applicationMapper;
    private final ConnectionMapper connectionMapper;
    private final ConnectionRepository connectionRepository;
    @Override
    public List<ApplicationDTO> getAllApplications() {
        System.out.println("U servisu");
        List<Application> applications = applicationRepository.findAll();
        return applicationMapper.applicationsToApplicationDTOs(applications);
    }

    @Override
    public ApplicationDTO addNewApplication(ApplicationDTO applicationDTO) {
        Application application = new Application();
        List<Connection> appConnections = connectionMapper.connectionDTOsToConnections(applicationDTO.getConnections());
        application.setName(applicationDTO.getName());
        applicationRepository.save(application);
        appConnections.stream().forEach( c ->
        {
            Connection connection = connectionRepository.findById(c.getId()).get();
            if (connection.getApplication() == null){
                connection.setApplication(application);
            }else{
                throw new BadRequestException("This connection already has application!");
            }
            connectionRepository.save(connection);
        });

        application.setConnections(appConnections);

        return applicationMapper.applicationToApplicationDTO(application);
    }

    @Override
    public ApplicationDTO deleteApplication(Long applicationId) {
        Application application = applicationRepository.findById(applicationId).get();
        if(application != null){
            List<Connection> appConnections = application.getConnections();
            for (Connection c:appConnections) {
                connectionRepository.delete(c);
            }
        }
        applicationRepository.delete(application);

        return applicationMapper.applicationToApplicationDTO(application);
    }

    @Override
    public ApplicationDTO editApplication(ApplicationDTO applicationDTO) {
        Application application = applicationRepository.findById(applicationDTO.getId()).get();
        if(application != null) {
            List<Connection> appConnections = connectionMapper.connectionDTOsToConnections(applicationDTO.getConnections());
            application.setId(applicationDTO.getId());
            application.setName(applicationDTO.getName());
            applicationRepository.save(application);
            appConnections.stream().forEach( c ->
            {
                Connection connection = connectionRepository.findById(c.getId()).get();
                connection.setApplication(application);
                connectionRepository.save(connection);
            });
            application.setConnections(connectionMapper.connectionDTOsToConnections(applicationDTO.getConnections()));
        }

        return applicationMapper.applicationToApplicationDTO(application);
    }

    @Override
    public ApplicationDTO getApplicationById(Long id) {
        Application application = applicationRepository.findById(id).get();

        return applicationMapper.applicationToApplicationDTO(application);
    }

    @Override
    public ConnectionDTO removeConnectionFromApplication(ConnectionDTO connectionDTO) {
        Connection connection = connectionRepository.findById(connectionDTO.getId()).get();
        if (connection != null){
            connection.setApplication(null);
        }else{
            throw new ConnectionException("Connection doesn't exist!");
        }
        connectionRepository.save(connection);

        return connectionMapper.connectionToConnectionDTO(connection);
    }
}

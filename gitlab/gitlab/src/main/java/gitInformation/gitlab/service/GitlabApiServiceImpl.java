package gitInformation.gitlab.service;

import gitInformation.gitlab.dto.CommitDTO;
import gitInformation.gitlab.dto.MergeRequestDTO;
import gitInformation.gitlab.dto.ProjectDTO;
import gitInformation.gitlab.model.Connection;
import gitInformation.gitlab.model.Project;
import gitInformation.gitlab.service.interface_service.GitlabApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GitlabApiServiceImpl implements GitlabApiService {
    @Override
    public Project getProject(Connection connection) {
        WebClient client = WebClient.create();
        WebClient.ResponseSpec responseSpec1 = client.get()
                .uri(connection.getUrl() + "/users/5630854" +"/projects?scope=projects&search="
                        + connection.getProject() + "&private_token=" + connection.getToken())
                .headers(h -> h.setBearerAuth(connection.getToken()))
                .retrieve();
        List<ProjectDTO> projects = responseSpec1.bodyToMono(new ParameterizedTypeReference<List<ProjectDTO>>() {})
                .block();
        Project project = new Project();
        projects.stream().forEach( p-> {
                project.setId(p.getId());
        });

        return project;
    }

    @Override
    public List<MergeRequestDTO> getMergeRequests(Connection connection, Project project){
        WebClient client = WebClient.create();
        //!prepravi na opened mr kasnije
        WebClient.ResponseSpec responseSpec = client.get()
                .uri(connection.getUrl()
                        + "/projects/"
                        + project.getId() + "/merge_requests?target_branch="
                        + connection.getBranchName() + "&state=all")
                .headers(h -> h.setBearerAuth(connection.getToken()))
                .retrieve();
        List<MergeRequestDTO> mergeRequests = responseSpec.bodyToMono(new ParameterizedTypeReference<List<MergeRequestDTO>>() {})
                .block();

        List<MergeRequestDTO> lastMergeRequests = mergeRequests.stream()
                    .sorted(Comparator.comparing(MergeRequestDTO::getCreated_at).reversed())
                    .limit(10)
                    .collect(Collectors.toList());

        return lastMergeRequests;
    }

    @Override
    public List<CommitDTO> getCommits(Connection connection, Project project) {
        WebClient client = WebClient.create();
        WebClient.ResponseSpec responseSpec = client.get()
                .uri(connection.getUrl()
                        + "/projects/"
                        + project.getId()
                        + "/repository/commits"
                        + "/?branch=" + connection.getBranchName())
                .headers(h -> h.setBearerAuth(connection.getToken()))
                .retrieve();
        List<CommitDTO> commits = responseSpec.bodyToMono(new ParameterizedTypeReference<List<CommitDTO>>() {})
                .block();

        List<CommitDTO> lastCommits = commits.stream()
                .sorted(Comparator.comparing(CommitDTO::getCreated_at).reversed())
                .limit(10)
                .collect(Collectors.toList());

        return lastCommits;
    }
}

package gitInformation.gitlab.service.interface_service;

import gitInformation.gitlab.dto.CommitDTO;
import gitInformation.gitlab.dto.MergeRequestDTO;
import gitInformation.gitlab.model.Connection;
import gitInformation.gitlab.model.Project;

import java.text.ParseException;
import java.util.List;

public interface GitlabApiService {
    Project getProject(Connection connection);
    List<MergeRequestDTO> getMergeRequests(Connection connection, Project project) throws ParseException;
    List<CommitDTO> getCommits(Connection connection, Project project);
}

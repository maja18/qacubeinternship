package gitInformation.gitlab.service;

import gitInformation.gitlab.dto.PersonDTO;
import gitInformation.gitlab.exceptions.UserNotExistsException;
import gitInformation.gitlab.mappers.PersonMapper;
import gitInformation.gitlab.model.Person;
import gitInformation.gitlab.model.Provider;
import gitInformation.gitlab.repository.PersonRepository;
import gitInformation.gitlab.service.interface_service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;
    private final PersonMapper personMapper;
    @Override
    public PersonDTO login(PersonDTO personDTO) {
        Person person = personRepository.findUserByEmailAndPassword(personDTO.getEmail(), personDTO.getPassword());
        if(person ==null){
            throw new UserNotExistsException("That user does not exist!");
        }

        return personMapper.personToPersonDTO(person);
    }

    @Override
    public PersonDTO processOAuthPostLogin(String email) {
        Person existPerson = personRepository.findUserByEmail(email);
        Person newPerson = new Person();
        if (existPerson == null) {
            newPerson.setEmail(email);
            newPerson.setProvider(Provider.GOOGLE);
            newPerson.setEnabled(true);
            personRepository.save(newPerson);
        }else
            return personMapper.personToPersonDTO(existPerson);

        return personMapper.personToPersonDTO(newPerson);
    }
}

package gitInformation.gitlab.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Setter
@Getter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ConnectionException extends RuntimeException{
    private String message;

    private ConnectionException(){}
    public ConnectionException(String message){
        super(message);
        this.message = message;
    }
}
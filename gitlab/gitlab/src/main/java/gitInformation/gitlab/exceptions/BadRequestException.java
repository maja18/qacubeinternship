package gitInformation.gitlab.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Setter
@Getter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends  RuntimeException{
    private String message;

    public BadRequestException(){}
    public BadRequestException(String message){
        super(message);
        this.message = message;
    }
}

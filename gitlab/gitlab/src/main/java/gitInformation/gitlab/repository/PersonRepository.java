package gitInformation.gitlab.repository;

import gitInformation.gitlab.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    Person findUserByEmailAndPassword(String email, String password);
    Person findUserByEmail(String email);
}

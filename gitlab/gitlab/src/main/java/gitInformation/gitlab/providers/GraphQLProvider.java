package gitInformation.gitlab.providers;

import gitInformation.gitlab.fetchers.GraphQLApplicationFetcher;
import gitInformation.gitlab.fetchers.GraphQLConnectionFetcher;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class GraphQLProvider {
    private static final String GRAPHQL_SCHEMA_FILE = "/application.graphqls";

    private GraphQL graphQL;

    @Autowired
    private GraphQLApplicationFetcher graphQLApplicationFetcher;

    @Autowired
    private GraphQLConnectionFetcher graphQLConnectionFetcher;

    @Bean
    public GraphQL graphQL() {
        return graphQL;
    }

    @PostConstruct
    public void init() throws URISyntaxException, IOException {
        Path graphQLSchemaFilePath = Paths.get(this.getClass().getResource(GRAPHQL_SCHEMA_FILE).toURI());;
        GraphQLSchema graphQLSchema = buildSchema(graphQLSchemaFilePath);
        this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    private GraphQLSchema buildSchema(Path graphQLSchemaFilePath) throws IOException {
        String graphQLSchema = new String(Files.readAllBytes(graphQLSchemaFilePath), StandardCharsets.UTF_8);
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(graphQLSchema);
        RuntimeWiring runtimeWiring = buildWiring();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
    }

    private RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("allApplications", graphQLApplicationFetcher.allApplications())
                        .dataFetcher("allConnections", graphQLConnectionFetcher.allConnections())
                )
                .build();
    }
}

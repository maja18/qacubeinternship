package gitInformation.gitlab.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CommitDTO {
    private String author_name;
    private String short_id;
    private String message;
    private String created_at;
}

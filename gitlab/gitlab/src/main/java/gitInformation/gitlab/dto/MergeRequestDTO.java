package gitInformation.gitlab.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@RequiredArgsConstructor
public class MergeRequestDTO {
    private String title;
    private AuthorDTO author;
    private AssigneeDTO assignee;
    private String created_at;
}

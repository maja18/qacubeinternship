package gitInformation.gitlab.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class ApplicationDTO {
    private Long id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    private List<ConnectionDTO> connections;
}

package gitInformation.gitlab.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ProjectDTO {
    private Long id;
    private String name;
}

package gitInformation.gitlab.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@RequiredArgsConstructor
public class ConnectionDTO {
    private Long id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotBlank(message = "Url is mandatory")
    private String url;

    @NotBlank(message = "Token is mandatory")
    private String token;

    @NotBlank(message = "Branch name is mandatory")
    private String branchName;

    @NotBlank(message = "Project name is mandatory")
    private String project;
}

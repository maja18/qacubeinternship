package gitInformation.gitlab.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@RequiredArgsConstructor
public class AuthorDTO {
    private String name;
}

package gitInformation.gitlab.controller;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import gitInformation.gitlab.dto.PersonDTO;
import gitInformation.gitlab.exceptions.BadRequestException;
import gitInformation.gitlab.service.PersonServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationController {
    private final PersonServiceImpl personService;

    @GetMapping
    public Map<String, Object> currentUser(OAuth2AuthenticationToken oAuth2AuthenticationToken){
        return oAuth2AuthenticationToken.getPrincipal().getAttributes();
    }

    @PostMapping("/login")
    public ResponseEntity<PersonDTO> login(@Valid @RequestBody PersonDTO personDTO) {
        PersonDTO person = personService.login(personDTO);

        return person == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(person);
    }

    @PostMapping("/loginGoogle")
    public ResponseEntity<PersonDTO> loginWithGoogle(@RequestBody String tokenId, @Autowired NetHttpTransport transport, @Autowired GsonFactory jsonFactory) throws GeneralSecurityException, IOException {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList("600786407464-llspkfklmqtirmmk3vuci8nanqlo1vd1.apps.googleusercontent.com"))
                .build();

        GoogleIdToken idToken = verifier.verify(tokenId);
        PersonDTO person;
        if (idToken != null) {
            GoogleIdToken.Payload payload = idToken.getPayload();
            String email = payload.getEmail();
            person = personService.processOAuthPostLogin(email);
        } else {
            throw new BadRequestException("Token is invalid!");
        }

        return person == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(person);
    }
}

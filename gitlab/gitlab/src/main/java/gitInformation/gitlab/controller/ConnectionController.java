package gitInformation.gitlab.controller;

import gitInformation.gitlab.dto.ConnectionDTO;
import gitInformation.gitlab.exceptions.ConnectionException;
import gitInformation.gitlab.providers.GraphQLProvider;
import gitInformation.gitlab.service.ConnectionServiceImpl;
import graphql.ExecutionResult;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/connection", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ConnectionController {

    private final ConnectionServiceImpl connectionService;

    @Autowired
    private GraphQLProvider graphQLProvider;

    @PostMapping
    public ResponseEntity<ConnectionDTO> addNewConnection(@Valid @RequestBody ConnectionDTO connectionDTO) {
        ConnectionDTO connection = connectionService.addNewConnection(connectionDTO);

        return connection == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(connection);
    }

    @GetMapping("/connections")
    ResponseEntity<List<ConnectionDTO>> getAllConnections()
    {
        List<ConnectionDTO> connections = connectionService.getAllConnections();

        return connections == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(connections);
    }

    @DeleteMapping(value = "/{connection-id}")
    public ResponseEntity<ConnectionDTO> deleteConnection(@PathVariable(name = "connection-id") Long connectionId) {
        ConnectionDTO connection = connectionService.deleteConnection(connectionId);

        return new ResponseEntity<>(connection, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ConnectionDTO> editConnection(@Valid @RequestBody ConnectionDTO connectionDTO) {
        ConnectionDTO connection = connectionService.editConnection(connectionDTO);

        return connection == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(connection);
    }

    @GetMapping(value = "/{connection-id}")
    ResponseEntity<ConnectionDTO> getConnectionById(@PathVariable(name = "connection-id") Long connectionId)
    {
        ConnectionDTO connection;
        try {
            connection = connectionService.getConnectionById(connectionId);
        }catch (Exception e){
            throw new ConnectionException("Connection with given id doesn't exist!");
        }

        return connection == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(connection);
    }

    @GetMapping("/availableConnections")
    ResponseEntity<List<ConnectionDTO>> getAllAvailableConnections()
    {
        List<ConnectionDTO> connections = connectionService.getAllAvailableConnections();

        return connections == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(connections);
    }

    @PostMapping("/graphql")
    public ResponseEntity<Object> findAllConnections(@RequestBody String query){
        ExecutionResult execute = graphQLProvider.graphQL().execute(query);
        return new ResponseEntity<>(execute, HttpStatus.OK);
    }
}

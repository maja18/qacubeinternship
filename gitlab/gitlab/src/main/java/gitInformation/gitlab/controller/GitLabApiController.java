package gitInformation.gitlab.controller;

import gitInformation.gitlab.dto.CommitDTO;
import gitInformation.gitlab.dto.MergeRequestDTO;
import gitInformation.gitlab.exceptions.BadRequestException;
import gitInformation.gitlab.model.Connection;
import gitInformation.gitlab.model.Project;
import gitInformation.gitlab.repository.ConnectionRepository;
import gitInformation.gitlab.service.GitlabApiServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/gitlab", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class GitLabApiController {
    private final ConnectionRepository connectionRepository;
    private final GitlabApiServiceImpl gitlabApiService;

    @GetMapping ("/merge-requests/{connection-id}")
    public ResponseEntity<List<MergeRequestDTO>> getMergeRequests(@PathVariable(name = "connection-id") Long connectionId) {
        Optional<Connection> connection = Optional.ofNullable(connectionRepository.findById(connectionId).get());
        List<MergeRequestDTO> mergeRequests;
        if(!connection.isEmpty()){
            try {
                Project project = gitlabApiService.getProject(connection.get());
                mergeRequests = gitlabApiService.getMergeRequests(connection.get(), project);
            }catch (RuntimeException exception){
                throw new BadRequestException("Bad request, please try again!");
            }

            return mergeRequests == null ?
                    new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(mergeRequests);
        }

        return null;
    }

    @GetMapping ("/commits/{connection-id}")
    public ResponseEntity<List<CommitDTO>> getCommits(@PathVariable(name = "connection-id") Long connectionId) {
        Optional<Connection> connection = Optional.ofNullable(connectionRepository.findById(connectionId).get());
        List<CommitDTO> commits;
        if(!connection.isEmpty()){
            try {
                Project project = gitlabApiService.getProject(connection.get());
                commits = gitlabApiService.getCommits(connection.get(), project);
            }catch (RuntimeException exception){
                throw new BadRequestException("Bad request, please try again!");
            }

            return commits == null ?
                    new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(commits);
        }

        return null;
    }
}

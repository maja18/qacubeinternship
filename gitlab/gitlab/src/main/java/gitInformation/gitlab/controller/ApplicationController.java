package gitInformation.gitlab.controller;

import gitInformation.gitlab.dto.ApplicationDTO;
import gitInformation.gitlab.dto.ConnectionDTO;
import gitInformation.gitlab.exceptions.BadRequestException;
import gitInformation.gitlab.providers.GraphQLProvider;
import gitInformation.gitlab.service.ApplicationServiceImpl;
import graphql.ExecutionResult;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/application", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ApplicationController {
    private final ApplicationServiceImpl applicationService;
    @Autowired
    private GraphQLProvider graphQLProvider;
    @GetMapping("/applications")
    ResponseEntity<List<ApplicationDTO>> getAllApplications()
    {
        List<ApplicationDTO> applications = applicationService.getAllApplications();

        return applications == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(applications);
    }

    @PostMapping
    public ResponseEntity<ApplicationDTO> addNewApplication(@Valid @RequestBody ApplicationDTO applicationDTO) {
        ApplicationDTO application = applicationService.addNewApplication(applicationDTO);

        return application == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(application);
    }

    @DeleteMapping(value = "/{application-id}")
    public ResponseEntity<ApplicationDTO> deleteApplication(@PathVariable(name = "application-id") Long applicationId) {
        ApplicationDTO application = applicationService.deleteApplication(applicationId);

        return new ResponseEntity<>(application, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ApplicationDTO> editApplication(@Valid @RequestBody ApplicationDTO applicationDTO) {
        ApplicationDTO application = applicationService.editApplication(applicationDTO);

        return application == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(application);
    }

    @GetMapping(value = "/{application-id}")
    ResponseEntity<ApplicationDTO> getApplicationById(@PathVariable(name = "application-id") Long applicationId)
    {
        ApplicationDTO application;
        try {
            application = applicationService.getApplicationById(applicationId);
        }catch (Exception e){
            throw new BadRequestException("Application with given id doesn't exist!");
        }

        return application == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(application);
    }

    @PutMapping("/removeConnection")
    public ResponseEntity<ConnectionDTO> removeConnectionFromApplication(@Valid @RequestBody ConnectionDTO connectionDTO) {
        ConnectionDTO connection = applicationService.removeConnectionFromApplication(connectionDTO);

        return connection == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(connection);
    }

    @PostMapping("/graphql")
    public ResponseEntity<Object> findAllApplications(@RequestBody String query){
        ExecutionResult execute = graphQLProvider.graphQL().execute(query);
        return new ResponseEntity<>(execute, HttpStatus.OK);
    }

}
